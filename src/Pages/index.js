import Auth from './Auth/Auth';
import Home from './Home/Home';
import WelcomePage from './WelcomePage/WelcomePage';
import WelcomePage2 from './WelcomePage2/WelcomePage2';
import Recommender0Page from './Recommender0Page/Recommender0Page';
import Recommender1Page from './Recommender1Page/Recommender1Page';
import Recommender2Page from './Recommender2Page/Recommender2Page';
import Recommender3Page from './Recommender3Page/Recommender3Page';
import RecommenderResults from './RecommenderResults/RecommenderResults';
import Details from './Details/Details';
import FilterProviders from './FilterProviders/FilterProviders';
import BuildingPage from './BuildingPage/BuildingPage';
import UserVerify from './UserVerify/UserVerify';
import FavoritesPage from './FavoritesPage/FavoritesPage';

export {
    WelcomePage,
    WelcomePage2,
    Auth,
    Home,
    Recommender0Page,
    Recommender1Page,
    Recommender2Page,
    Recommender3Page,
    RecommenderResults,
    Details,
    FilterProviders,
    BuildingPage,
    UserVerify,
    FavoritesPage
};
