import Loading from './Loading/Loading';
import RegisterForm from './RegisterForm/RegisterForm';
import LoginForm from './LoginForm/LoginForm';
import Header from './Header/Header';
import Carousel from './Carousel/Carousel';
import CarouselFilter from './CarouselFilter/CarouselFilter';
import NavbarBottom from './NavbarBottom/NavbarBottom';
import TopFilter from './TopFilter/TopFilter';
import AllMovies from './AllMovies/AllMovies';
import UserSettings from './UserSettings/UserSettings';
import SecureRoute from './SecureRoute/SecureRoute';
import FindMovie from './FindMovie/FindMovie';
import EmailConfirmation from './EmailConfirmation/EmailConfirmation';

export {
    RegisterForm,
    LoginForm,
    Loading,
    Header,
    Carousel,
    CarouselFilter,
    NavbarBottom,
    TopFilter,
    AllMovies,
    UserSettings,
    SecureRoute,
    FindMovie,
    EmailConfirmation,
};
